import os
import sys
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'geoalchemy2==0.5.0',
    'geojson==2.4.1',
    'pandas==0.23.3',
    'psycopg2==2.7.5',
    'pyramid==1.10.4',
    'pyramid-tm',
    'pyramid-jwtauth==0.1.3',
    'reportlab==3.4.0',
    'shapely==1.7.0',
    'sqlalchemy==1.3.17',
    'sqlalchemy-utils==0.30.11',
    'transaction==2.1.2',
    'waitress==1.4.4'

    ]

setup(name='ecoreleve_be_server',
      version='1.1',
      description='ecoReleve_be_Server',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='ecoreleve_server',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = ecoreleve_be_server:main
      [console_scripts]
      initialize_ecoReleve_Server_db = ecoreleve_be_server.scripts.initializedb:main
      """,
      )
