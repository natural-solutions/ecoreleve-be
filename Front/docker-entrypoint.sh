#!/bin/sh

if [[ ! -z "${NS_ECORELEVE_BE_FRONT_CORE_URL}" ]]; then
  sed -i "s|%core_url%|${NS_ECORELEVE_BE_FRONT_CORE_URL}|g" /usr/share/nginx/html/app/config.js
fi

if [[ ! -z "${NS_ECORELEVE_BE_FRONT_PORTAL_URL}" ]]; then
  sed -i "s|%portal_url%|${NS_ECORELEVE_BE_FRONT_PORTAL_URL}|g" /usr/share/nginx/html/app/config.js
fi


nginx -g "daemon off;"
